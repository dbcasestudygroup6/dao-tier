from flask import Flask, Response, jsonify
import requests
from flask_cors import CORS
import json
import mysql.connector
from mysql.connector import Error
import threading
from config import *

app = Flask(__name__)
CORS(app)


def connect():
    connection = mysql.connector.connect(host='databaseserver.datagen-tier.svc.cluster.local',
                                     database='sampledb',
                                     user='userN8X',
                                     password='7mEpRwP1JCHYpquR', autocommit=True)
    return connection


def exract_instrument_deals(deal_list, instrument):
    new_deal_list = []
    for deal in deal_list:
        if deal['instrumentName'] == instrument:
            new_deal_list.append(deal)
    return new_deal_list


def find_current_price(instrument_deal_list):
    price_b = 0
    price_s = 0
    for deal in instrument_deal_list:
        if deal['type'] == 'B':
            price_b = deal['price']
        if deal['type'] == 'S':
            price_s = deal['price']

    return float(price_b), float(price_s)


def find_ave_buy(instrument_deal_list):
    total = 0
    count = 0
    for deal in instrument_deal_list:
        if deal['type'] == 'B':
            count += 1
            total += float(deal['price'])
    return total/count


def find_ave_sell(instrument_deal_list):
    total = 0
    count = 0
    for deal in instrument_deal_list:
        if deal['type'] == 'S':
            count += 1
            total += float(deal['price'])
    return total/count


def find_realized(instrument_deal_list):
    balance = 0
    for deal in instrument_deal_list:
        if deal['type'] == 'B':
            buy_quantity = int(deal['quantity'])
            buy_price = float(deal['price'])
            balance -= buy_quantity*buy_price
        if deal['type'] == 'S':
            sell_quantity = int(deal['quantity'])
            sell_price = float(deal['price'])
            balance += sell_quantity*sell_price

    return balance


def find_effective(realised_profit, position):
    return float(realised_profit) + float(position)


def find_position(instrument_deal_list, current_buy, current_sell):
    buy_quantity = 0
    sell_quantity = 0
    for deal in instrument_deal_list:
        if deal['type'] == 'B':
            buy_quantity += int(deal['quantity'])
            price_b = deal['price']
        if deal['type'] == 'S':
            sell_quantity += int(deal['quantity'])
            price_s = deal['price']

    if buy_quantity > sell_quantity:
        position = (buy_quantity-sell_quantity) * current_sell
        # use sell price
    else:
        position = (buy_quantity - sell_quantity) * current_buy
        # use buy price
    return position


@app.route('/get_data')
def return_data():
    all_deals = extract_all_from_db() # python dictionary/json form
    print(all_deals)
    send_data = []

    connection = connect()
    cursor = connection.cursor()

    sql_select_unique_instruments_query = ("SELECT instrument_name FROM instrument")
    cursor.execute(sql_select_unique_instruments_query)
    unique_instruments = cursor.fetchall()

    cursor.close()
    connection.close()


    # # to be replaced with a dynamic look up on the SQL database
    # instrument_list = ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
    #                "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"]

    for instrument in unique_instruments:
        instrument = instrument[0]
        instrument_deal_list = exract_instrument_deals(all_deals, instrument=instrument)
        # print(instrument_deal_list)

        current_buy, current_sell = find_current_price(instrument_deal_list)
        # print(current_buy, current_sell)

        average_buy = find_ave_buy(instrument_deal_list)
        average_sell = find_ave_sell(instrument_deal_list)
        # print('ave buy {}, ave sell {}'.format(average_buy, average_sell))

        realized_profit = find_realized(instrument_deal_list)

        position = find_position(instrument_deal_list, current_buy, current_sell)
        # print('postion = {}'.format(position))

        effective_profit = find_effective(realized_profit, position)
        # print('realised prof: {},  effective profit: {}'.format(realized_profit, effective_profit))


        send_data.append({'instrumentName': instrument,
                          'effective':effective_profit,
                          'realized':realized_profit,
                          'avgBuy': average_buy,
                          'avgSell': average_sell,
                          'currentBuy': current_buy,
                          'currentSell': current_sell,
                          'position': position})

    json_output = {}
    for count, value in enumerate(send_data):
        json_output[count] = value

    print(json_output)
    return jsonify(json_output)


def extract_all_from_db():
    sql_select_Query = "select * from deal ORDER BY deal_id DESC LIMIT 100"
    result = execute_request(sql_select_Query)
    print("Total number of deals in deals table is - ", len(result))
    deal_list = []


    for count, i in enumerate(result):
        # translate cpty and instrument id to actual values via sql query
        connection = connect()
        cursor = connection.cursor()

        sql_select_cpty_query = ("SELECT MIN(cpty_name) AS cpty FROM counter_party WHERE cpty_id = %s")
        cursor.execute(sql_select_cpty_query, (i[2],))
        cpty_record = cursor.fetchall()
        cpty = cpty_record[0][0]

        sql_select_instrument_query = (
            "SELECT MIN(instrument_name) AS instrument FROM instrument WHERE instrument_id = %s")
        cursor.execute(sql_select_instrument_query, (i[1],))
        instrument_record = cursor.fetchall()
        instrument = instrument_record[0][0]

        cursor.close()
        connection.close()


        deal_list.append({'dealId' : i[0],
            'instrumentName' : instrument,                     #call instrument name
            'cpty' : cpty,
            'price' : i[3],
            'type' : i[4],
            'quantity' : i[5],
            'time' : i[6]})
        # print(deal_list[count])
    return deal_list

def execute_request(command):
    connection = connect()
    try:
        if connection.is_connected():
            print('connected')
            cursor = connection.cursor()
            cursor.execute(command)
            records = cursor.fetchall()
    except Error as e:
        print("Error while connecting to MySQL", e)
    finally:
        # closing database connection.
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")
    return records

####### DB insertions below

@app.route('/')
def stream_listen():
    messages = requests.get('http://' + HOST + ':8090/streamTest', stream=True)
    def eventStream():
        for msg in messages.iter_lines(chunk_size=1):
            if msg:
                new_str = msg.decode('utf-8')
                # logic for saving to database
                insert_counterparty_to_db(new_str)
                insert_to_db(new_str)
                yield new_str
    return Response(eventStream(), mimetype="text/event-stream")


def insert_counterparty_to_db(deal):
    connection = connect()
    try:
        if connection.is_connected():
            # print('connected')
            cursor = connection.cursor()
            json_deal = json.loads(deal)

            sql_insert_into_cpty_table_query = ("INSERT IGNORE INTO counter_party "  # ignores insert duplicates 
                                                "(cpty_name)"
                                                "VALUES (%(cpty)s)")
            counterparty_data = {
                'cpty': json_deal['cpty']}

            result_cpty_data = cursor.execute(sql_insert_into_cpty_table_query, counterparty_data)
            connection.commit()


    except mysql.connector.Error as error:
        connection.rollback()  # rollback if any exception occured
        print("Failed inserting record into deals_database table {}".format(error))

    finally:
        # closing database connection.
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            # print("MySQL connection is closed")


def insert_to_db(deal):
    connection = connect()
    try:
        if connection.is_connected():
            # print('connected')
            cursor = connection.cursor()

            json_deal = json.loads(deal)

            sql_insert_into_deal_table_query = ("INSERT INTO deal "
                                "(deal_id, instrument_id, cpty_id, price, deal_type, quantity, deal_time)"
                                "VALUES (%(deal_id)s, %(instrument_id)s, %(cpty_id)s, %(price)s, %(deal_type)s, %(quantity)s, %(deal_time)s) ")

            sql_insert_into_instrument_table_query = ("INSERT IGNORE INTO instrument "
                                          "(instrument_id, instrument_name)"
                                          "VALUES (%(instrument_id)s, %(instrument_name)s)")

            sql_select_cpty_id_query = ("SELECT MIN(cpty_id) AS Unique_Id FROM counter_party WHERE cpty_name = %s")

            cursor.execute(sql_select_cpty_id_query, (json_deal['cpty'],))
            record = cursor.fetchall()

            deal_data = {
                'deal_id': json_deal['dealId'],
                'instrument_id': json_deal['instrumentId'],
                'instrument_name': json_deal['instrumentName'],
                'cpty': json_deal['cpty'],
                'price': json_deal['price'],
                'deal_type': json_deal['type'],
                'quantity': json_deal['quantity'],
                'deal_time': json_deal['time'],
                'cpty_id': record [0][0]
                #'cpty_id': ("SELECT MIN (cpty_id) AS Unique_Id FROM counter_party WHERE cpty_name = %(cpty)s")
                }

            result_instrument_data = cursor.execute(sql_insert_into_instrument_table_query, deal_data)
            connection.commit()

            result_deal_data = cursor.execute(sql_insert_into_deal_table_query, deal_data)
            connection.commit()
            # print("Record inserted successfully into deals_database table")

    except mysql.connector.Error as error:
        connection.rollback()  # rollback if any exception occured
        print("Failed inserting record into deals_database table {}".format(error))

    finally:
        # closing database connection.
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            # print("MySQL connection is closed")


@app.before_first_request
def activate_function():
    def run_job():
        stream_listen()
    thread = threading.Thread(target=run_job)
    thread.start()


def start_runner():
    def start_loop():
        not_started = True
        while not_started:
            print('In start loop')
            try:
                r = requests.get('http://' + HOST + ':8080/')
                if r.status_code == 200:
                    print('Server started, quiting start_loop')
                    not_started = False
                print(r.status_code)
            except:
                print('Server not yet started')

    print('Started runner')
    thread = threading.Thread(target=start_loop)
    thread.start()


if __name__ == "__main__":
    start_runner() # THIS function listens and writes to database
    app.run(port=8080, threaded=True, host=(DOCKER_HOST))
    return_data()


