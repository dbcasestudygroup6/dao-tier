import pytest
from flask_dao import *

deal_list_stub = [{'dealId': 20120, 'instrumentName': 'Astronomica', 'cpty': 'Lewis', 'price': '3399.5259772912586', 'type': 'S', 'quantity': '43', 'time': '13-Aug-2019 (11:05:35.544567)'}, {'dealId': 20122, 'instrumentName': 'Astronomica', 'cpty': 'Lina', 'price': '1808.0285431736927', 'type': 'S', 'quantity': '559', 'time': '13-Aug-2019 (11:05:35.728999)'}, {'dealId': 20123, 'instrumentName': 'Floral', 'cpty': 'John', 'price': '393.99649501760825', 'type': 'S', 'quantity': '208', 'time': '13-Aug-2019 (11:05:35.843725)'}]
only_Astronomica_deals = [{'dealId': 20120, 'instrumentName': 'Astronomica', 'cpty': 'Lewis', 'price': '3399.5259772912586', 'type': 'S', 'quantity': '2', 'time': '13-Aug-2019 (11:05:35.544567)'},
                          {'dealId': 20109, 'instrumentName': 'Astronomica', 'cpty': 'Lewis', 'price': '2', 'type': 'S', 'quantity': '2', 'time': '13-Aug-2019 (11:05:35.544567)'},
                          {'dealId': 20122, 'instrumentName': 'Astronomica', 'cpty': 'Lina', 'price': '1808.0285431736927', 'type': 'B', 'quantity': '1', 'time': '10-Aug-2019 (11:05:35.728999)'},
                          {'dealId': 20123, 'instrumentName': 'Astronomica', 'cpty': 'Lina', 'price': '1', 'type': 'B', 'quantity': '1', 'time': '13-Aug-2019 (11:05:35.728999)'}]


print(len(deal_list_stub))

# current buy = find_current_buy(instrument_deal_list)
# current sell = find_current_sell(instrument_deal_list)
# realized_profit = find_realised(instrument_deal_list)
# effective_profit = find_effective(instrument_deal_list)
# average_buy = find_ave_buy(instrument_deal_list)
# average_sell = find_ave_sell(instrument_deal_list)
# position = find position (instrument_deal_list)

def test_extract_instrument_deals():
    instrument_deal_list = exract_instrument_deals(deal_list_stub, instrument='Astronomica')
    for i in instrument_deal_list:
        assert(i['instrumentName'] == 'Astronomica')
    return

def test_find_current_price():
    Astronomica_current_buy, sell = find_current_price(only_Astronomica_deals)
    print(Astronomica_current_buy)
    assert(Astronomica_current_buy == 1)
    assert(sell == 2)
    return


def test_type_realized_profit():
    realized_profit = find_realized(only_Astronomica_deals)
    print(realized_profit)
    assert(type(realized_profit) == float)
    return

def test_type_effective_profit():
    effective_profit = find_effective(1.1, 1.1)
    assert(type(effective_profit) == float)
    return

def test_type_find_ave_buy():
    average_buy = find_ave_buy(only_Astronomica_deals)
    assert(type(average_buy) == float)
    return

def test_type_find_ave_sell():
    average_sell = find_ave_sell(only_Astronomica_deals)
    assert(type(average_sell) == float)
    return

def test_type_find_position():
    position = find_position(only_Astronomica_deals, 1.1, 1.1)
    assert(type(position) == float)
    return